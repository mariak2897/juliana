/* Author: Khedwala Maria

*/


'use strict'

window.onload = function() {

	var body = document.querySelector("body");

	// script for hamburger overlay
	var mainHam = document.querySelector(".hamburger");
	var closeMenu = document.querySelector(".close-menu");

	// opening script
	mainHam.addEventListener("click",function() {
	  var navMenu = document.querySelector(".overlay");
	  navMenu.classList.add("noncollapse");
	  navMenu.classList.remove("collapse");
	  body.classList.add("hideExtra");
	});	
	// closing script
	closeMenu.addEventListener("click",function() {
	  var menuOverlay = document.querySelector(".overlay")
	  menuOverlay.classList.add("collapse");
	  menuOverlay.classList.remove("noncollapse");
	  body.classList.remove("hideExtra");
	});

	var form = document.querySelector("form");
	var closeSearch = document.querySelector(".close-search");
	var searchMain = document.querySelector(".search-main");
	
	searchMain.addEventListener("click",function() {
	  form.classList.add("active");
	  closeSearch.classList.add("active");
	});
	closeSearch.addEventListener("click",function() {
		form.classList.remove("active");
		closeSearch.classList.remove("active");
	})


	// script for slider plugin
  $(".flexslider").flexslider({
    animation: "slide"
  });
}();



















